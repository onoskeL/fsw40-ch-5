const http = require('http')

const genarateToken = (value) => {
  if (value === 'Bearer is_customer') return 2
  else if (value === 'Bearer is_admin') return 1
  else return null
}

const server = http.createServer((req, res) => {
  const { method, url, headers } = req

  const role = genarateToken(headers.authorization)

  try {
  } catch (err) {
    res.writeHead(500).end(err)
  }
})

const port = 3000
const hostname = '127.0.0.1'

server.listen(port, hostname, () => {
  console.log(`Server berjalan di http://${hostname}:${port}`)
})
